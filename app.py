
from flask import Flask, request, jsonify
from json import loads, load
from sys import path
path.append('src/')
from commands import sonda
import os



app = Flask(__name__)
sonda = sonda()
METHODS = ['PUT', 'POST', 'GET', 'DELETE']

# Reseta a sonda para a posição (0,0)
@app.route('/iniciar_sonda', methods=METHODS)
def reset():
    if(request.method != 'PUT'):
        return jsonify({"sucesso": False, "mensagen": "Utilize o metodo PUT"})
    else:
        sonda.gerarMatriz()
        return "Vamos resetar a sonda", 200

# Move a sonda
@app.route('/mover_sonda', methods=METHODS)
def mover_sonda():
    if(request.method != "POST"):
        return jsonify({"sucesso": False, "mensagen": "Utilize o metodo POST"})
    else:
        result = sonda.recebeCommands(request.get_json()['movimentos'])
        if(result['succeso']):
            return jsonify({"sucesso": True, 'x': result['x'], 'y': result['y']})
        else:
            return jsonify({"erro": "Um movimento inválido foi detectado, infelizmente a sonda ainda não possui a habilidade de #vvv"})

#Pega a posiçao atual da sonda
@app.route('/posicao_atual', methods=METHODS)
def posicao_atual():
    if(request.method != "GET"):
        return jsonify({"sucesso": False, "mensagen": "Utilize o metodo GET"})
    else:
        return jsonify(sonda.pegaAtualPosition())

port = int(os.environ.get("PORT", 5000))
app.run(host='0.0.0.0', port=port)