### Rodando localmente.

Para se rodar localmente, basta rodar um 
```pip install -r requirements.txt```
e logo em seguida rodar 
``` python app.py``` 

### EndPoints

Foi criado 3 endpoist
1. ```/iniciar_sonda```   
    Mandando uma requisição PUT você reseta x e y para 0,0
2. ```/mover_sonda```  
    Mandando uma requisição POST com um json com a seguinte estrutura  
    ``` json {
                "movimentos": [
                    "GE",
                    "M",
                    "M",
                    "M",
                    "GD",
                    "M",
                    "M"
                ]
            } 
3. ```/posicao_atual```   
    Pega a posição atual da sonda.


### Deploy 

A aplicação também pode ser encontrada rodando em: https://sondacredere.herokuapp.com/mover_sonda