import unittest
from sys import path
path.append('src/')
from commands import sonda


class VerificarPangramaTests(unittest.TestCase):

    def test_err_sond(self):
        test = sonda().recebeCommands(['GD', 'M', 'M'])
        self.assertFalse(test)

    def test_sonda(self):
        test = sonda().recebeCommands(['GE', 'M', 'M', 'M', 'GD', 'M', 'M'])
        self.assertTrue(test)