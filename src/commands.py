# -*- coding=UTF-8 -*-

import numpy as np

class sonda:
    app = []
    x = 4
    y = 0
    xx = 0 
    yy = 0   
    def __init__(self):
        self.gerarMatriz()

    def gerarMatriz(self):
        self.app = np.chararray((5,5), itemsize=2, unicode=True)
        self.app[:] = 'x'
        self.app[4][0] = 'D'
    
    def recebeCommands(self, commads):   
        for command in commads:
            if(command in ['GE', 'GD']):
                self.app[self.x][self.y] = str(self.checkPosition(self.app[self.x][self.y], command))

            if(command == 'M'):
                result = self.move(self.app[self.x][self.y], self.app, self.x, self.y)
                if(result['success']):
                    self.x = result['x']
                    self.y = result['y']
                else:
                    self.x = result['x']
                    self.y = result['y']
                    return {'sucesso': False}
        return {'succeso': True, 'x': self.yy, 'y': self.xx}

    def move(self, direction, app, x, y):
        #Para cima podemos falar que ((x-1), y)
        if(direction == 'C'):
            self.x -= 1
            if(self.checkIfLocal(self.x, self.y)):
                self.app[self.x + 1][self.y] = 'x'
                self.app[self.x][self.y] = 'C'
                self.xx += 1 
                return {'success': True,'x': self.x, 'y': self.y}
            else:
                return {'success': False, 'x': (self.x + 1), 'y': self.y}

        # Para baixo podemos falar que ((x+1), y)
        if(direction == 'B'):
            self.x += 1
            if(self.checkIfLocal(self.x, self.y)):
                self.app[self.x - 1][self.y] = 'x'
                self.app[self.x][self.y] = 'B'
                self.xx -= 1
                return {'success': True,'x': self.x, 'y': self.y}
            else:
                return {'success': False, 'x': (self.x - 1), 'y': self.y}

        #Podemos dizer que para esquerda (x, (y - 1))
        if(direction == 'E'):
            self.y -= 1
            if(self.checkIfLocal(self.x, self.y)):
                self.app[self.x][self.y - 1] = 'x'
                self.app[self.x][self.y] = 'E'
                self.yy += 1
                return {'success': True,'x': self.x, 'y': self.y}
            else:
                return {'success': False, 'x': self.x, 'y': (self.y - 1)}

        #Para direita podemos dizer que (x, (y + 1))
        if(direction == 'D'):
            self.y += 1 
            if(self.checkIfLocal(self.x, self.y)):
                self.app[self.x][self.y - 1] = 'x'
                self.app[self.x][self.y] = 'D'
                self.yy += 1
                return {'success': True,'x': self.x, 'y': self.y}
            else:
                return {'success': False, 'x': self.x, 'y': (self.y - 1)}
        
    def checkIfLocal(self, x, y):
        if(self.x > 4 or self.x < 0 or self.y > 4 or self.y < 0):
            return False
        else:
            return True

    def checkPosition(self, actualPosition, newRotate):
        if(newRotate == 'GE'):
            if(actualPosition == 'C'):
                return 'E'
            elif (actualPosition == 'B'): 
                return 'E'
            elif(actualPosition == 'D'):
                return 'C'
            elif(actualPosition == 'E'):
                return "B"
        else:
            if(actualPosition == 'C'):
                return 'D'
            elif(actualPosition == 'B'):
                return 'D'
            elif(actualPosition == 'D'):
                return 'B'
            elif(actualPosition == 'E'):
                return 'C'

    def pegaAtualPosition(self,):
        return {'x': self.yy, 'y': self.xx, 'face': self.app[self.x][self.y]}